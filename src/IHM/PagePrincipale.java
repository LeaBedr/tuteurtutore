package IHM;

import java.awt.Paint;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import classes.Algo;
import classes.Etudiant;
import classes.FormatException;
import classes.ImportStudentsFromFile;
import classes.Tuteur;
import classes.Tutore;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class PagePrincipale {

	@FXML 
	VBox affectes;
	
	@FXML
	VBox nonAffectes;
	
	ArrayList<Etudiant> tuteurs = new ArrayList<Etudiant>();
	ArrayList<Etudiant> tutores = new ArrayList<Etudiant>();
	ArrayList<GridPane> pane = new ArrayList<GridPane>();
	ArrayList<VBox> vboxes = new ArrayList<VBox>();
	Algo algo = new Algo();
	Map<Tuteur, List<Tutore>> affectation=new HashMap<Tuteur,List<Tutore>>();
	Label dragged;
	
	int[] ponderation=new int[] {1,1,1,1};
	
    public void initialiserEtudiants() {
		ArrayList<Etudiant> etudiants;
		try {
			etudiants = ImportStudentsFromFile.importStudent("res/etudiants");
		} catch (FormatException e) {
			System.out.println(e.getMessage());
			initialiserEtudiants();
			return;
		}
		for(Etudiant e : etudiants) {
			if(e.getNiveau()==1) {
				tutores.add(e);
			}else {
				tuteurs.add(e);
			}
		}
		affectation=algo.affecter(tuteurs, tutores, ponderation);
    }

    
    public void initialize() {
    	
        System.out.println("Initialisation...");
        initialiserEtudiants();
        int i=1;
        int j=0;
       
        for(Etudiant tutore : tutores) {
            boolean present = false;
            for(List<Tutore> tutores : affectation.values()) {
            //for(Etudiant tutor : tutores) {
                if (tutores.contains(tutore)) {
                    present = true;
                }
            //}
            }
            if (!present) {
            	Label tutoreNonAffecte=new Label();
                tutoreNonAffecte.setText(tutore.getPrenom()+" "+tutore.getNom());
                tutoreNonAffecte.setPrefHeight(20);
                tutoreNonAffecte.setPrefWidth(150);
                tutoreNonAffecte.setBorder(new Border(new BorderStroke(Color.valueOf("#000000"), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, null)));
                nonAffectes.getChildren().add(tutoreNonAffecte);
                VBox.setMargin(tutoreNonAffecte, new Insets(10,10,10,10));
            }
        }
        
        for(Tuteur t:affectation.keySet()) {
        	for (Tutore tutore : affectation.get(t)) {
        		t.addTutore(tutore);
			}
        	VBox vbox=new VBox();
        	vbox.setBorder(new Border(new BorderStroke(Color.valueOf("#009A00"), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, null)));
        	VBox.setMargin(vbox, new Insets(10,10,10,10));
        	GridPane grid=new GridPane();
            Label nom = new Label();
            nom.setBorder(new Border(new BorderStroke(Color.valueOf("#00009A"), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, null)));
        	nom.setText(t.getPrenom()+" "+t.getNom());
        	nom.setPrefHeight(20);
           	nom.setPrefWidth(120);
           	String[] tuteurData = new String[] {
    				t.getNom(),
    				t.getPrenom(),
    				"" + t.getAge(),
    				t.getMail()
    		};
           	nom.addEventHandler(MouseEvent.MOUSE_CLICKED, e ->{
           		Stage stage=new Stage();
                FXMLLoader loader = new FXMLLoader();
                URL fxmlFileUrl = getClass().getResource("info_tuteur.fxml");
                if (fxmlFileUrl == null) {
                        System.out.println("Impossible de charger le fichier fxml");
                        System.exit(-1);
                }
                loader.setLocation(fxmlFileUrl);
                BorderPane root;
				try {
					root = loader.load();
	                Scene scene = new Scene(root);
	                VBox center = (VBox) root.getCenter();
					ObservableList<Node> list = center.getChildren();
					for (int index = 0; index < list.size(); index++) {
						Node node = list.get(index);
						HBox hBox = (HBox) node;
						ObservableList<Node> hBoxChildren = hBox.getChildren();
						for(Node node2 : hBoxChildren) {
							if(node2 instanceof TextField) {
								TextField textField = (TextField) node2;
								textField.setText(tuteurData[index]);
							}
						}
					}
					HBox bottom = (HBox) root.getBottom();
					ListView<Label> listView = (ListView<Label>) bottom.getChildren().get(1);
					for (Etudiant etudiant : t.getTutores()) {
						listView.getItems().add(new Label(etudiant.getNom() + " " + etudiant.getPrenom()));
					}
					listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Label>() {
						@Override
						public void changed(ObservableValue<? extends Label> observable, Label oldValue, Label newValue) {
							System.out.println("Selected item: " + newValue.getText());
						}
					});
	                stage.setScene(scene);
	                stage.setTitle("Tuteur");
	                stage.show();
				} catch (IOException e1) {

					e1.printStackTrace();
				}
           	});
           	
        	grid.add(nom, 0, 0);
        	GridPane.setMargin(nom, new Insets(10,10,10,10));
        	vbox.getChildren().add(grid);
        	pane.add(grid);

        	vboxes.add(vbox);
        	i=1;
        	for(Tutore tutore : t.getTutores()) {
        		String[] tutoreData = new String[] {
        				tutore.getNom(),
        				tutore.getPrenom(),
        				"" + tutore.getAge(),
        				tutore.getMail()
        		};
        		
        		if (i>2) {
        			i=0;
        			j++;
        		}
        		Label nomtutore = new Label();
            	nomtutore.setText(tutore.getPrenom()+" "+tutore.getNom());
            	nomtutore.setPrefHeight(20);
               	nomtutore.setPrefWidth(120);
            	nomtutore.setBorder(new Border(new BorderStroke(Color.valueOf("#9E9E9E"), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, null)));
               	nomtutore.addEventHandler(MouseEvent.MOUSE_CLICKED, e ->{
               		Stage stage=new Stage();
                    FXMLLoader loader = new FXMLLoader();
                    URL fxmlFileUrl = getClass().getResource("info_tutoré.fxml");
                    if (fxmlFileUrl == null) {
                            System.out.println("Impossible de charger le fichier fxml");
                            System.exit(-1);
                    }
                    loader.setLocation(fxmlFileUrl);
                    BorderPane root;
    				try {
    					root = loader.load();
    					VBox center = (VBox) root.getCenter();
    					ObservableList<Node> list = center.getChildren();
    					for (int index = 0; index < list.size(); index++) {
    						Node node = list.get(index);
							HBox hBox = (HBox) node;
							ObservableList<Node> hBoxChildren = hBox.getChildren();
							for(Node node2 : hBoxChildren) {
								if(node2 instanceof TextField) {
									TextField textField = (TextField) node2;
									textField.setText(tutoreData[index]);
								}
							}
						}
    					HBox bottom = (HBox) root.getBottom();
    					ListView<Label> listView = (ListView<Label>) bottom.getChildren().get(1);
    					for (Etudiant etudiant : tutore.getTuteurs().keySet()) {
							listView.getItems().add(new Label(etudiant.getNom() + " " + etudiant.getPrenom()));
						}
    					
    					VBox right = (VBox) root.getRight();
    					ObservableList<Node> rightList = right.getChildren();
    					ListView<Label> availableTuteurs = (ListView<Label>) rightList.get(1);
    					for (Etudiant tuteur : tuteurs) {
							if(tuteur.getNom() != null) availableTuteurs.getItems().add(new Label(tuteur.getNom() + " " + tuteur.getPrenom()));
						}
    					
    					availableTuteurs.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Label>() {
    						@Override
    						public void changed(ObservableValue<? extends Label> observable, Label oldValue, Label newValue) {
    							System.out.println("Selected item: " + newValue.getText());
    							boolean ok=false;
    							for(Label l : listView.getItems()) {
    								if(!newValue.getText().equals(l.getText())) {
    									ok=true;
    								}
    							}
    							if(ok) {
    								listView.getItems().add(newValue);
	    							String[] tokens=newValue.getText().split(" ");
	    							for (Etudiant tuteur : tuteurs) {
	    								if(tuteur.getNom() != null && tuteur.getNom().equals(tokens[0]) && tuteur.getPrenom().equals(tokens[1])) {
	    									tutore.addTuteur((Tuteur) tuteur);
	    									System.out.println("tuteur ajouté");
	    								}
	    							}
	    							System.out.println(tutore.getTuteurs());
    							}
    						}
    					});
    					
    					/*HBox top = (HBox) root.getTop();
    					ObservableList<Node> topList = top.getChildren();
    					TextField tuteurName = (TextField) topList.get(1);
    					tuteurName.setText()*/
    	                Scene scene = new Scene(root);
    	                stage.setScene(scene);
    	                stage.setTitle("Tutore");
    	                stage.show();
    				} catch (IOException e1) {
    					// TODO Auto-generated catch block
    					e1.printStackTrace();
    				}
               	});
        		grid.add(nomtutore, i, j);

        		GridPane.setMargin(nomtutore, new Insets(10,10,10,10));
        		i++;
        	}
        	for(Node n : nonAffectes.getChildren()) {
        		((Label)n).setOnDragDetected(e -> {
        			dragged=(Label) n;
	            	System.out.println("DnD detecté.");
	            	final Dragboard dragBroard =n.startDragAndDrop(TransferMode.MOVE);
	            	final ClipboardContent content = new ClipboardContent();      
	                final WritableImage capture = n.snapshot(null, null); 
	                content.putImage(capture); 
	            	dragBroard.setContent(content);
	            	e.consume();
        		});
        	
        	}
        	grid.setOnDragDropped(new EventHandler<DragEvent>() {
        	    public void handle(DragEvent event) {
        	        Dragboard db = event.getDragboard();
        	        System.out.println("ok");
        	        event.setDropCompleted(true);
        	        grid.add(dragged, 3, 3);
        	        event.consume();
        	     }
        	});
        	//drag n drop pas réussi
        }
        for(VBox v : vboxes) {
        	affectes.getChildren().add(v);
        }
    }
    
    
    public void boutonAffecter() throws IOException {
    	Stage stage=new Stage();
        FXMLLoader loader = new FXMLLoader();
        URL fxmlFileUrl = getClass().getResource("affectation.fxml");
        if (fxmlFileUrl == null) {
                System.out.println("Impossible de charger le fichier fxml");
                System.exit(-1);
        }
        loader.setLocation(fxmlFileUrl);
        Parent root = loader.load();

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Affectation");
        stage.show(); 
    }
    
}
