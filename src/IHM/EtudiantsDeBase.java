package IHM;
import classes.Enseignant;
import classes.Etudiant;
import classes.Ressource;
import classes.Tuteur;
import classes.TuteurDeuxiemeAnnee;
import classes.TuteurTroisiemeAnnee;
import classes.Tutore; 

public class EtudiantsDeBase {
	
	Tutore tutore1, tutore2, tutore3;
	Tuteur tuteur1, tuteur2, tuteur3, tuteur4;
	Enseignant enseignant1, enseignant2, enseignant3;
	
	public void initialize() {
		tutore1=new Tutore("Lea", "Bedrici", 18, "lea.bedrici.etu@univ-lille.fr",0,15.2,7,1);
		tutore2=new Tutore("Paul-Louis", "Gomis", 18, "paul-louis.gomis.etu@univ-lille.fr", 0,15.2,0,1);
		tutore3=new Tutore("Younes", "Chahid", 18, "younes.chahid.etu@univ-lille.fr");
		tuteur1=new TuteurTroisiemeAnnee("Jean", "Dupont", 20, "jean.dupont.etu@univ-lille.fr");
		tuteur2=new TuteurTroisiemeAnnee("Louis", "Dubois", 20, "louis.dubois.etu@univ-lille.fr");
		tuteur3=new TuteurTroisiemeAnnee("Mathilde", "Rico", 20, "mathilde.rico.etu@univ-lille.fr");
		tuteur4=new TuteurDeuxiemeAnnee("Charlie","Nelson",19,"charlie.nelson.etu@univ-lille.fr",0,18,8);
		enseignant1 = new Enseignant("Antoine", "Nongaillard", 40, "antoire.nongaillard.ens@univ-lille.fr");
		enseignant2 = new Enseignant("Cindy", "Cappelle", 35, "cindy.cappelle.ens@univ-lille.fr");
		enseignant3 = new Enseignant("Iovka", "Boneva", 37, "iovka.boneva.ens@univ-lille.fr");
		
	
		
		tutore1.addAbsence(3);
		tutore1.addRessource(Ressource.GPO);
		tutore1.addRessource("maths");
		tutore2.setMotivation(7);
		tutore2.addRessource("anglais");
		tutore3.setMoyenne(15.5);
		tutore3.setMotivation(7);
		tutore3.setNiveau(1);
		tutore3.addAbsence(2);
		tutore3.addRessource("ecoNoMIE");
		
		tuteur2.setNiveau(3);
	}
	
}
