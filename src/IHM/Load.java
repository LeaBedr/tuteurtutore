package IHM;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import classes.Enseignant;
import classes.Ressource;
import classes.Tuteur;
import classes.TuteurTroisiemeAnnee;
import classes.Tutore;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Load extends Application {
	
	@FXML 
	AnchorPane affectes; 
	
	@FXML
	AnchorPane nonAffectes;
	
	ArrayList<Tuteur> tuteurs = new ArrayList<Tuteur>();
	ArrayList<GridPane> pane = new ArrayList<GridPane>();
	
    public void start(Stage stage) throws IOException {
            FXMLLoader loader = new FXMLLoader();
            URL fxmlFileUrl = getClass().getResource("principal.fxml");
            if (fxmlFileUrl == null) {
                    System.out.println("Impossible de charger le fichier fxml");
                    System.exit(-1);
            }
            loader.setLocation(fxmlFileUrl);
            Parent root = loader.load();

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("SAE");
            

            int posY=0;
            for(Tuteur t:tuteurs) {
            	GridPane grid=new GridPane();
                Label nom = new Label();
            	nom.setText(t.getPrenom()+" "+t.getNom());
            	grid.add(nom, 0, 0);
            	posY+=200;
            	grid.setLayoutY(posY);
            	pane.add(grid);
            }
            stage.show();
    }

    public static void main(String[] args) {
            Application.launch(args);
    }
}