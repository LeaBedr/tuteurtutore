package IHM;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;

import classes.ImportStudentsFromFile;
import classes.Tutore;
import classes.Etudiant;
import classes.FormatException;

import java.util.ArrayList;

import classes.Algo;

/**
 * 
 */

/**
 * @author paullouis.gomis.etu
 *
 */
public class ControllerPonderation {
	@FXML
    Slider sliderMoyenne;
	@FXML
	Label labelMoyenne;
	@FXML
    Slider sliderMotivation;
	@FXML
	Label labelMotivation;
	@FXML
    Slider sliderAbsence;
	@FXML
	Label labelAbsence;
	@FXML
    Slider sliderAnnee;
	@FXML
	Label labelAnnee;
	@FXML
	Button boutonAffecter;
	
	
	Algo algo = new Algo();
	ArrayList<Etudiant> etudiants;
	ArrayList<Etudiant> tutores = new ArrayList<Etudiant>();
	ArrayList<Etudiant> tuteurs = new ArrayList<Etudiant>();
	
	void getStudents() {
		try {
			etudiants = ImportStudentsFromFile.importStudent();
		} catch (FormatException e) {
			e.getMessage();
			getStudents();
		}
	}

    public void initialize() {
    		getStudents();
            System.out.println("Initialisation...");
            for (Etudiant e : etudiants) {
            	if (e instanceof Tutore) {
            		tutores.add(e);
            	} else {
            		tuteurs.add(e);
            	}
            }
            
            sliderMoyenne.valueProperty().addListener((o,old,newValue) -> {
            	double d = newValue.doubleValue();
                labelMoyenne.setText("" + String.format("%3.0f", d).substring(2));
            });
            sliderMotivation.valueProperty().addListener((o,old,newValue) -> {
            	double d = newValue.doubleValue();
                labelMotivation.setText("" + String.format("%3.0f", d).substring(2));
            });
            sliderAbsence.valueProperty().addListener((o,old,newValue) -> {
            	double d = newValue.doubleValue();
                labelAbsence.setText("" + String.format("%3.0f", d).substring(2));
            });
            sliderAnnee.valueProperty().addListener((o,old,newValue) -> {
            	double d = newValue.doubleValue();
                labelAnnee.setText("" + String.format("%3.0f", d).substring(2));
            });
            
            boutonAffecter.addEventHandler(ActionEvent.ACTION, e -> {
            	int[] poids=new int[4];
            	poids[0] = Integer.parseInt (labelMoyenne.getText());
            	poids[1] = Integer.parseInt (labelMotivation.getText());
            	poids[2] = Integer.parseInt (labelAbsence.getText());
            	poids[3] = Integer.parseInt (labelAnnee.getText());
            	algo.affecter(tuteurs, tutores,poids);
            });
        
    }   

    public void slidertoLabel(MouseEvent event) {
            int newValue = (int) sliderMoyenne.getValue();
            labelMoyenne.setText(String.format("%3.2f", newValue));
            
    }
}
