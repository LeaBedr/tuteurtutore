package classes;
/**
 * 
 */

/**
 * @author Younes Chahid
 * @version 10 mai 2022 15:34:50
 */
public class FormatException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String DEFAULTFORMAT = "nom,prenom,age,mail,abscences,moyenne,motivation,année,ressources...";
	public static final String DEFAULTVERIF = "veuillez vérifier que vous avez correctement appliqué le format de données";
	static final String DEFAULTMESSAGE;
	static final String NEWLINE = System.getProperty("line.separator");
	static {
		StringBuilder sb = new StringBuilder();
		sb.append("Une erreur est survenue lors de la lecture du fichier, ");
		sb.append(DEFAULTVERIF);
		sb.append(NEWLINE);
		sb.append(DEFAULTFORMAT);
		DEFAULTMESSAGE = sb.toString();
	}

	/**
	 * 
	 */
	public FormatException() {
		this(DEFAULTMESSAGE);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public FormatException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public FormatException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public FormatException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public FormatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
