package classes;
import java.util.Objects;

public abstract class Personne {
	/**
	 * @author Lea Bedrici, Younes Chahid, Paul Louis Gomis
	 * 
	 */
	private String prenom; 
	/** le nom de la personne**/
	private String nom;
	/** le prénom de la personne **/
	private int age;
	/** l'âge de la personne **/
	private String mail;
	/** l'adresse mail de la personne **/
	
	public Personne(String nom, String prenom, int age, String mail) {
		this.nom=nom;
		this.prenom=prenom;
		this.age=age;
		this.mail=mail;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	@Override
	public String toString() {
		return nom + " "+ prenom +": " + "age: " + age + ", mail: " + mail;
	}

	@Override
	public int hashCode() {
		return Objects.hash(age, mail, nom, prenom);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Personne other = (Personne) obj;
		return age == other.age && Objects.equals(mail, other.mail) && Objects.equals(nom, other.nom)
				&& Objects.equals(prenom, other.prenom);
	}
}
