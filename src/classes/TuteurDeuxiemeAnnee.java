package classes;
import java.util.ArrayList;

public class TuteurDeuxiemeAnnee extends Tuteur{
	
	private Tutore t;

	public TuteurDeuxiemeAnnee(String nom, String prenom, int age, String mail,int nb, double moy, int motiv, Ressource ressource) {
		super(nom, prenom, age, mail,nb,moy,motiv, 2, ressource);	
	}
	public TuteurDeuxiemeAnnee(String nom, String prenom, int age, String mail,int nb, double moy, int motiv) {
		super(nom, prenom, age, mail,nb,moy,motiv);	
	}

	public TuteurDeuxiemeAnnee(String nom, String prenom, int age, String mail) {
		super(nom, prenom, age, mail);
	}
	
	@Override
	public ArrayList<Tutore> getTutores() {

		ArrayList<Tutore> res = new ArrayList<Tutore>();
		res.add(t);
		return res;
	}

	@Override
	public void addTutore(Tutore t) {
		if (this.getTutores().get(0)!=null) {
			this.getTutores().get(0).removeTuteur(this);
		}
		this.t=t;
		t.addTuteur(this);
	}

}
