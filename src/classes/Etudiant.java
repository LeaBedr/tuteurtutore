package classes;

import java.util.List;
import java.util.Objects;

public abstract class Etudiant extends Personne{
	private int nbAbsences; 
	/** le nombre d'absences de l'étudiant **/
	private double moyenne; 
	/** la moyenne de l'étudiant **/
	private int motivation;
	/** la motivation de l'étudiant sur une échelle de 10 (donnée par le professeur)**/
	private int niveau;
	/** le niveau d'études: 1, 2 ou 3 pour première, deuxième et troisième année **/
	private boolean valide;
	
	public Etudiant(String nom, String prenom, int age, String mail, int nb, double moy, int motiv, int niveau) {
		super(nom,prenom,age,mail);
		this.nbAbsences=nb;
		this.moyenne=moy;
		this.motivation=motiv;
		this.niveau=niveau;
	}
	
	public Etudiant(String nom, String prenom, int age, String mail, int nb, double moy, int motiv) {
		this(nom,prenom,age,mail,nb, moy, motiv, 0);
	}

	public Etudiant(String nom, String prenom, int age, String mail) {
		this(nom,prenom,age,mail,0,0,0,0);
	}
	
	public int getNbAbsences() {
		return nbAbsences;
	}
	public void setNbAbsences(int nbAbsences) {
		this.nbAbsences = nbAbsences;
	}
	public double getMoyenne() {
		return moyenne;
	}
	public void setMoyenne(double moyenne) {
		this.moyenne = moyenne;
	}
	public int getMotivation() {
		return motivation;
	}
	public void setMotivation(int motivation) {
		this.motivation = motivation;
	}
	public int getNiveau() {
		return niveau;
	}
	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}
	
	public void addAbsence(int nb) {
		nbAbsences+=nb;
	}
	public void updateMoyenne(double moy) {
		moyenne=moy;
	}
	public void updateMotivation(int motiv) {
		motivation=motiv;
	}
	public String toString() {
		return super.toString() + ", année " + niveau + ", nbAbsences= " + nbAbsences +", moyenne= "+moyenne+", motivation="+motivation;
	}

	public boolean getValide() {
		return this.valide;
	}
	
	public void setValide(boolean val) {
		this.valide=val;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(motivation, moyenne, nbAbsences, niveau);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Etudiant other = (Etudiant) obj;
		if(getNom() == null && other.getNom() == null) return false;
		return motivation == other.motivation
				&& Double.doubleToLongBits(moyenne) == Double.doubleToLongBits(other.moyenne)
				&& nbAbsences == other.nbAbsences && niveau == other.niveau;
	}
	
	public abstract List<Ressource> getRessources();
	
	
	public String getRawData() {
		return getNom() + "," + getPrenom() + "," + getAge() + "," + getNbAbsences() + "," + getMoyenne() + "," + getMotivation() + "," + getNiveau() + ressourcesToString(getRessources());
	}
	
	private String ressourcesToString(List<Ressource> ressources) {
		String res = "";
		for (int i = 0; i < ressources.size(); i++) {
			res+= "," + ressources.get(i);
		}
		return res;
	}
}
