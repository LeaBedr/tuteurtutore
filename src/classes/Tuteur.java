package classes;
import java.util.ArrayList;
import java.util.List;

public abstract class Tuteur extends Etudiant{
	
	private Ressource participe;
	
	public Tuteur(String nom, String prenom, int age, String mail, int nb, double moy, int motiv, int niveau, Ressource participe) {
		super(nom,prenom,age,mail,nb,moy,motiv,niveau);
		this.participe=participe;
	}
	
	public Tuteur(String nom, String prenom, int age, String mail, int nb, double moy, int motiv, int niveau) {
		super(nom,prenom,age,mail,nb,moy,motiv,niveau);
	}
	
	public Tuteur(String nom, String prenom, int age, String mail, int nb, double moy, int motiv) {
		super(nom,prenom,age,mail,nb,moy,motiv);
	}
	
	public Tuteur(String nom, String prenom, int age, String mail) {
		this(nom,prenom,age,mail,0,0,0,0, null);
	}
	
	public abstract ArrayList<Tutore> getTutores();
	
	public Ressource getRessource() {
		return participe;
	}
	
	public String toString() {
		String res= "Tuteur: "+ super.toString() + ", participe: ";
		if(participe!=null) {
			res+=participe.toString();
		}
		else {
			res+="pas de ressource choisie";
		}
		
		if(!getValide()) {
			res+=" !!! Cette candidature n'a pas été validée";
		}
		return res;
	}
	
	public void setRessource(Ressource r) {
		this.participe=r;
	}
	
	public void setRessource(String r) {
		this.participe=Ressource.valueOf(r.toUpperCase());
	}
	
	public abstract void addTutore(Tutore t);
	


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((participe == null) ? 0 : participe.hashCode());
		result = prime * result + (getValide() ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Tuteur))
			return false;
		Tuteur other = (Tuteur) obj;
		if (participe != other.participe)
			return false;
		if (getValide() != other.getValide())
			return false;
		return true;
	}

	@Override
	public List<Ressource> getRessources() {
		ArrayList<Ressource> res = new ArrayList<Ressource>();
		res.add(participe);
		return res;
	}
	
}
