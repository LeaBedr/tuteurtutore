package classes;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 
 */

/**
 * @author Younes Chahid
 * @version 18 mai 2022 09:38:01
 */
public class MainMenuMethods{
	// pondération: [ note, motivation, absence, année ] 
	private static final String[] PONDERATION = new String[] { 
			"note",
			"motivation",
			"absence",
			"annee",
	};
	
	static private void returnToMainMenu() {
		System.out.println("Appuyez sur entrée pour revenir au menu principal");
		Main.read();
	}
	
	static void affecter() {
		ArrayList<Etudiant> tuteurs = new ArrayList<>(Main.tuteurs);
		ArrayList<Etudiant> tutores = new ArrayList<>(Main.tutores);
		for(int i = tuteurs.size() - 1; i > -1; i--) {
			if(!tuteurs.get(i).getValide()) tuteurs.remove(i);
		}
		for(int i = tutores.size() - 1; i > -1; i--) {
			if(!tutores.get(i).getValide()) tutores.remove(i);
		}
		Main.algo.affecter(Main.tuteurs, Main.tutores, Main.poids);
	}
	
	static void importFromFile() {
		ArrayList<Etudiant> students;
		try {
			students = ImportStudentsFromFile.importStudent();
		} catch (FormatException e) {
			System.out.println(e.getMessage());
			returnToMainMenu();
			return;
		}
		for (Etudiant etudiant : students) {
			if(etudiant instanceof Tutore) {
				Main.tutores.add(etudiant);
			}
			else {
				Main.tuteurs.add(etudiant);
			}
		}
	}
	
	static void editWeight() {
		// TODO modification de la gestion des poids dans l'algorithme
		System.out.println("Entrez le paramètre que vous souhaitez modifier ainsi que son nouveau poids sous la forme:");
		System.out.println("contrainte:poids contrainte2:poids etc...");
		System.out.println("Voici la liste des contraintes");
		System.out.println("note, motivation, absence, annee");
		String rawData = Main.read();
		String[] contraintData = rawData.split(" ");
		int[] nouvellePonderation = Arrays.copyOf(Main.poids, 4);
		for (String constraint : contraintData) {
			String constraintName = constraint.split(":")[0];
			String constraintValue = constraint.split(":")[1];
			int realConstraintValue;
			try {
				realConstraintValue = Integer.parseInt(constraintValue);
			} catch (NumberFormatException e) {
				System.out.println("La valeur de contrainte \"" + constraintValue + "\" n'est pas un nombre pour la contrainte \""+ constraintName + "\"");
				returnToMainMenu();
				return;
			}
			int index = Arrays.asList(PONDERATION).indexOf(constraintName.toLowerCase());
			if(index == -1) {
				System.out.println("La contrainte \"" + constraintName + "\" n'existe pas");
				System.out.println("Rappel des contraintes: " + Arrays.toString(PONDERATION));
				returnToMainMenu();
				return;
			}
			nouvellePonderation[index] = realConstraintValue;
		}
		for (int i = 0; i < nouvellePonderation.length; i++) {
			Main.poids[i] = nouvellePonderation[i];
		}
	}
	
	
	
	
	
	static void addStudent() {
		System.out.println("Veuillez entrer les données d'un étudiant sous la forme suivante:");
		System.out.println("nom,prenom,age,mail,absences,moyenne,motivation,année,ressources...");
		try {
			String line = Main.read();
			String[] data = line.split(",");
			ImportStudentsFromFile.check(data, 1);
		}
		catch(FormatException e) {
			System.out.println(e.getMessage());
			Main.sleep(5);
			Main.clear();
			addStudent();
		}
	}
	
	
	
	
	static void sauvegarderAffectation() {
		System.out.println("Veuillez entrer le chemin absolu vers le dossier de destination");
		String path = Main.read();
		File folder = new File(path);
		if(!folder.exists()) {
			System.out.println("Ce dossier n'existe pas");
			Main.sleep(3);
			sauvegarderAffectation();
			return;
		}
		System.out.println("Veuillez entrer le nom du fichier dans lequel enregistrer l'affectation");
		String fileName = Main.read();
		File file = new File(folder.getAbsolutePath() + "/" + fileName);
		if(!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Impossible de creer le fichier \"" + fileName + "\" dans le dossier \"" + folder.getAbsolutePath() + "\"");
				System.out.println("Retour au menu principal");
			}
		}
		try {
			SaveData.saveMap(Main.algo, file);
		} catch (IOException e) {
			System.out.println("Impossible de sauvegarder dans le fichier \"" + file.getAbsolutePath() + "\", retour au menu principal");
			Main.sleep(4);
			return;
		}
	}
	
	
	
	
	
	static void showStudents() {
		for (Etudiant tuteur : Main.tuteurs) {
			System.out.println(tuteur);
		}
		for (Etudiant tutore : Main.tutores) {
			System.out.println(tutore);
		}
		System.out.println("Appuyez sur entrée pour revenir au précédent principal");
		Main.read();
		return;
	}
	
	static void showListStudents(List<Etudiant> etudiants) {
		for (int i = 0; i < etudiants.size(); i++) {
			System.out.println("" + i + ". " + etudiants.get(i));
		}
	}
	
	
	
	
	
	
	static void deleteStudent() {
		deleteStudentMenu();
		int max = 3;
		int choix = 0;
		String entry = Main.read();
		try {
			choix = Integer.parseInt(entry);
		}
		catch(Exception e) {
			System.out.println("Vous devez entrer un nombre entier");
			deleteStudent();
			return;
		}
		if(choix > max || choix < 1) {
			System.out.println("Vous devez entrer un nombre entre 1 et " + max);
			deleteStudent();
			return;
		}
		switch (choix) {
			case 1:
				showStudents();
				break;
			case 2:
				ArrayList<Etudiant> etudiants = searchStudent();
				showListStudents(etudiants);
				System.out.println("Entrez le numéro associé a l'étudiant que vous souhaitez supprimer ou \"retour\" si vous souhaitez annuler");
				entry = Main.read();
				if(entry.toLowerCase().equals("retour")) return;
				try {
					choix = Integer.parseInt(entry);
				}
				catch(Exception e) {
					System.out.println("Vous devez entrer un nombre entier");
					deleteStudent();
					return;
				}
				if(choix > etudiants.size() || choix < 1) {
					System.out.println("Vous devez entrer un nombre entre 1 et " + etudiants.size());
					deleteStudent();
					return;
				}
				Etudiant toDelete = etudiants.get(choix - 1);
				if(toDelete instanceof Tutore) {
					Main.tutores.remove(toDelete);
				}
				else {
					Main.tuteurs.remove(toDelete);
				}
				break;
		}
	}
	
	
	
	
	
	
	private static ArrayList<Etudiant> searchStudent() {
		ArrayList<Etudiant> res = new ArrayList<Etudiant>();
		System.out.println("Entrez un nom d'étudiant");
		String name = Main.read();
		for (Etudiant etudiant : Main.tuteurs) {
			if(etudiant.getNom().startsWith(name)) {
				res.add(etudiant);
			}
		}
		for (Etudiant etudiant : Main.tutores) {
			if(etudiant.getNom().startsWith(name)) {
				res.add(etudiant);
			}
		}
		return res;
	}

	/**
	 * 
	 */
	private static void deleteStudentMenu() {
		System.out.println("1. Afficher les etudiants");
		System.out.println("2. Chercher un étudiant");
		System.out.println("3. Retour au menu principal");
	}
	
	
	static void validerUnEtudiant() {
		menuDeValidation();
		int max = 3;
		int choix = 0;
		String entry = Main.read();
		try {
			choix = Integer.parseInt(entry);
		}
		catch(Exception e) {
			System.out.println("Vous devez entrer un nombre entier");
			deleteStudent();
			return;
		}
		if(choix > max || choix < 1) {
			System.out.println("Vous devez entrer un nombre entre 1 et " + max);
			deleteStudent();
			return;
		}
		switch (choix) {
			case 1:
				showStudents();
				break;
			case 2:
				ArrayList<Etudiant> etudiants = searchStudent();
				showListStudents(etudiants);
				System.out.println("Entrez le numéro associé a l'étudiant que vous souhaitez valider ou \"retour\" si vous souhaitez annuler");
				entry = Main.read();
				if(entry.toLowerCase().equals("retour")) return;
				try {
					choix = Integer.parseInt(entry);
				}
				catch(Exception e) {
					System.out.println("Vous devez entrer un nombre entier");
					validerUnEtudiant();
					return;
				}
				if(choix > etudiants.size() || choix < 1) {
					System.out.println("Vous devez entrer un nombre entre 1 et " + etudiants.size());
					validerUnEtudiant();
					return;
				}
				Etudiant aValider = etudiants.get(choix - 1);
				Main.enseignant.valider(aValider);
				if(!aValider.getValide()) System.out.println("Vous ne pouvez pas valider cet étudiant");
				returnToMainMenu();
				break;
		}
	}
	
	private static void menuDeValidation() {
		deleteStudentMenu();
	}
	
	
	
}
