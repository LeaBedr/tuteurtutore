package classes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Tutore extends Etudiant {

	private ArrayList<Ressource> apprends;
	


	private Map<Tuteur, Ressource> tuteurs;
	
	public Tutore(String nom, String prenom, int age, String mail, int nb, double moy, int motiv, int niveau, ArrayList<Ressource> apprends) {
		super(nom,prenom,age,mail,nb,moy,motiv,niveau);
		this.apprends=apprends;
		tuteurs=new HashMap<Tuteur, Ressource>();
	}
	
	public Tutore(String nom, String prenom, int age, String mail, int nb, double moy, int motiv, int niveau) {
		this(nom,prenom,age,mail,nb,moy,motiv,niveau, new ArrayList<Ressource>());
		tuteurs=new HashMap<Tuteur, Ressource>();
	}

	public Tutore(String nom, String prenom, int age, String mail) {
		this(nom,prenom,age,mail,0,0,0,0, new ArrayList<Ressource>());
	}
	
	public Map<Tuteur, Ressource> getTuteurs() {
		return tuteurs;
	}
	
	public ArrayList<Ressource> getApprends() {
		return apprends;
	}
	
	private String apprends() {
		String res="";
		for(Ressource r: apprends) {
			res+=r.toString()+" ";
		}
		return res;
	}
	
	public String toString() {
		String res= "Tutoré: "+ super.toString() + ", apprends: ";
		if(apprends.size()!=0) {
			res+=apprends();
		}
		else {
			res+="pas de ressources choisies";
		}
		if(!getValide()) {
			res+=" !!! Cette candidature n'a pas été validée";
		}
		return res;
	}
	
	public void addTuteur(Tuteur t) {
		tuteurs.put(t, t.getRessource());
	}
	
	public void addRessource(Ressource r) {
		this.apprends.add(r);
	}
	
	public void addRessource(String r) {
		this.apprends.add(Ressource.valueOf(r.toUpperCase()));
	}
	
	public void removeTuteur(Tuteur t) {
		tuteurs.remove(t);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((apprends == null) ? 0 : apprends.hashCode());
		result = prime * result + ((tuteurs == null) ? 0 : tuteurs.hashCode());
		result = prime * result + (getValide() ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Tutore))
			return false;
		Tutore other = (Tutore) obj;
		if (apprends == null) {
			if (other.apprends != null)
				return false;
		} else if (!apprends.equals(other.apprends))
			return false;
		if (tuteurs == null) {
			if (other.tuteurs != null)
				return false;
		} else if (!tuteurs.equals(other.tuteurs))
			return false;
		if (getValide() != other.getValide())
			return false;
		return true;
	}

	@Override
	public List<Ressource> getRessources() {
		return apprends;
	}
}
