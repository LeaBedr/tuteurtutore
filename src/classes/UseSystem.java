package classes;
public class UseSystem {
	
	
	public static void main(String[] args) {
		String nl=System.getProperty("line.separator");
		Tutore tutore1=new Tutore("Lea", "Bedrici", 18, "lea.bedrici.etu@univ-lille.fr",0,15.2,7,1);
		Tutore tutore2=new Tutore("Paul-Louis", "Gomis", 18, "paul-louis.gomis.etu@univ-lille.fr", 0,15.2,0,1);
		Tutore tutore3=new Tutore("Younes", "Chahid", 18, "younes.chahid.etu@univ-lille.fr");
		Tuteur tuteur1=new TuteurDeuxiemeAnnee("Jean", "Dupont", 20, "jean.dupont.etu@univ-lille.fr");
		Tuteur tuteur2=new TuteurDeuxiemeAnnee("Louis", "Dubois", 20, "louis.dubois.etu@univ-lille.fr");
		Tuteur tuteur3=new TuteurTroisiemeAnnee("Mathilde", "Rico", 20, "mathilde.rico.etu@univ-lille.fr");
		Enseignant enseignant1 = new Enseignant("Antoine", "Nongaillard", 40, "antoire.nongaillard.ens@univ-lille.fr");
		Enseignant enseignant2 = new Enseignant("Cindy", "Cappelle", 35, "cindy.cappelle.ens@univ-lille.fr");
		Enseignant enseignant3 = new Enseignant("Iovka", "Boneva", 37, "iovka.boneva.ens@univ-lille.fr");
		
		enseignant1.addRessource(Ressource.POO);
		enseignant1.addRessource(Ressource.BDD);
		enseignant2.addRessource("ijava");
		enseignant3.addRessource(Ressource.GRAPHES);
		
		System.out.println(tutore1 + nl + tutore2 + nl + tutore3);
		System.out.println(tuteur1 + nl + tuteur2 + nl + tuteur3);
		tutore1.addAbsence(3);
		tutore1.addRessource(Ressource.GPO);
		tutore1.addRessource("maths");
		tutore2.setMotivation(7);
		tutore2.addRessource("anglais");
		tutore3.setMoyenne(15.5);
		tutore3.setMotivation(7);
		tutore3.setNiveau(1);
		tutore3.addAbsence(2);
		tutore3.addRessource("ecoNoMIE");
		enseignant1.valider(tutore1);
		enseignant2.valider(tuteur1);
		enseignant3.valider(tutore2);
		enseignant2.valider(tuteur2);
		enseignant2.valider(tuteur3);
		System.out.println(tutore1 + nl + tutore2 + nl + tutore3);
		tuteur1.setPrenom("Valentin");
		tuteur1.setMail("valentin.dupont.etu@univ-lille.fr");
		tuteur2.setRessource(Ressource.GRAPHES);
		tuteur3.setRessource("outils");
		tuteur2.setNiveau(3);
		System.out.println(tuteur1 + nl + tuteur2 + nl + tuteur3);
		System.out.println(enseignant1 + nl + enseignant2 + nl + enseignant3);
		enseignant1.invalider(tuteur1);
		System.out.println(tuteur1);
	}
}
