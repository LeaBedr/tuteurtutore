package classes;

import java.io.IOException;
import java.net.URL;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class PagePrincipaleTest {
		
		
    @FXML
    AnchorPane affectes; 
    @FXML
    AnchorPane nonAffectes; 
    
    

    public void initialize() {
            System.out.println("Initialisation...");
    }
    


    public void boutonAffecter() throws IOException {
    	Stage stage=new Stage();
        FXMLLoader loader = new FXMLLoader();
        URL fxmlFileUrl = getClass().getResource("affectation.fxml");
        if (fxmlFileUrl == null) {
                System.out.println("Impossible de charger le fichier fxml");
                System.exit(-1);
        }
        loader.setLocation(fxmlFileUrl);
        Parent root = loader.load();

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Affectation");
        stage.show(); 
    }
    
}


