package classes;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * 
 */

/**
 * @author Younes Chahid
 * @version 9 mai 2022 16:30:52
 */
public class Import {
	static File file = new File("./res/format");
	static ArrayList<Tutore> tutores = new ArrayList<>();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Ressource[] values = Ressource.values();
		for (Ressource ressource : values) {
			System.out.println(ressource.name().equals("POO"));
		}
		try {
			BufferedReader example = new BufferedReader(new FileReader(file));
			String s = "";
			try {
				while((s = example.readLine()) != null) {
					if(!s.startsWith("//")) {
						// nom: 0 / prenom: 1 / age: 2 / mail: 3 / abscences: 4 / moyenne: 5 / 
						// motivation: 6 / année: 7 / ressources: 8+
						String[] data = s.split(",");
						ArrayList<Ressource> ressources = new ArrayList<Ressource>();
						for (int i = 8; i < data.length; i++) {
							String ressourceName = data[i].toUpperCase();
							System.out.println(data[i] + " " + data[i].toUpperCase());
							Ressource ressource = Ressource.valueOf(ressourceName);
							ressources.add(ressource);
						}
						System.out.println(ressources);
						System.out.println(Arrays.toString(data));
						int age = Integer.parseInt(data[2]);
						int abscences = Integer.parseInt(data[4]);
						double moyenne = Double.parseDouble(data[5]);
						int motivation = Integer.parseInt(data[6]);
						int annee = Integer.parseInt(data[7]);
						Tutore tutore = new Tutore(data[0], data[1], age, data[3], abscences, moyenne, motivation, annee, ressources);
						tutores.add(tutore);
					}
					System.out.println(s);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(tutores);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	static boolean contains(String[] array, String value) {
		for (String string : array) {
			if(string.equals(value)) return true;
		}
		return false;
	}

}
