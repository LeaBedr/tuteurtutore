package classes;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;



/**
 * 
 */

/**
 * @author Younes Chahid
 * @version 10 mai 2022 15:22:05
 */
public class ImportStudentsFromFile {
	
	public static ArrayList<Etudiant> importStudent() throws FormatException {
		System.out.println("Entrez un chemin de fichier ou entrez 'defaut' pour récupérer le fichier d'étudiants par défaut");
		String path = Main.read();
		if(path.equals("defaut")) {
			path="res/etudiants";
		}
		return importStudent(path);
	}

	public static ArrayList<Etudiant> importStudent(String path) throws FormatException {
		ArrayList<Etudiant> etudiants = new ArrayList<>();
		try(BufferedReader fileBr = new BufferedReader(new FileReader(new File(path)))) {
			ArrayList<String[]> data = read(fileBr);
			for (String[] etuData : data) {
				int age = Integer.parseInt(etuData[2]);
				int abscences = Integer.parseInt(etuData[4]);
				double moyenne = Double.parseDouble(etuData[5]);
				int motivation = Integer.parseInt(etuData[6]);
				int annee = Integer.parseInt(etuData[7]);
				ArrayList<Ressource> ressources = getRessources(etuData);
				Etudiant etudiant;
				if(annee == 1) {
					etudiant = new Tutore(etuData[0], etuData[1], age, etuData[3], abscences, moyenne, motivation, annee, ressources);
				}

				else if(annee==2) {
					etudiant = new TuteurDeuxiemeAnnee(etuData[0], etuData[1], age, etuData[3], abscences, moyenne, motivation, ressources.get(0));
				}else {
					etudiant = new TuteurTroisiemeAnnee(etuData[0], etuData[1], age, etuData[3], abscences, moyenne, motivation, ressources.get(0));
				}
				etudiants.add(etudiant);
			}
		}
		catch(FileNotFoundException e) {
			System.out.println("Ce fichier n'existe pas");
			return importStudent();
		}
		catch(IOException e) {
			System.out.println("Une erreur inconnue s'est produite");
			System.exit(1);
		}
		return etudiants;
	}
	
	public static ArrayList<Ressource> getRessources(String[] data) {
		ArrayList<Ressource> res = new ArrayList<>();
		for (int i = 8; i < data.length; i++) {
			res.add(Ressource.valueOf(data[i].toUpperCase()));
		}
		return res;
	}
	
	public static ArrayList<String[]> read(BufferedReader fileBr) throws FormatException, IOException {
		String line = "";
		ArrayList<String[]> array = new ArrayList<>();
		int ligne = 1;
		while ((line = fileBr.readLine()) != null) {
			if(!line.startsWith("//") && line.length() > 0) {
				String[] data = line.split(",");
				if(data.length < 9) {
					String textError = "Des données sont manquantes a la ligne " + ligne;
					textError += ", " + FormatException.DEFAULTVERIF;
					textError += FormatException.NEWLINE + FormatException.DEFAULTFORMAT;
					throw new FormatException(textError);
				}
				check(data, ligne);
				array.add(data);			
			}
			ligne++;	
		}
		return array;
		
	}
	
	public static void check(String[] data, int ligne) throws FormatException {
		int i = 8;
		try {
			while(i < data.length) {
				Ressource.valueOf(data[i].toUpperCase());
				i++;
			}
		}
		catch(IllegalArgumentException e) {
			String textError = "La ressource \"" + data[i] + "\" à la ligne " + ligne + " n'existe pas";
			textError += FormatException.NEWLINE + "Veuillez vérifier que vous avez correctement orthographié la ressource";
			textError += FormatException.NEWLINE + "Rappel, voici la liste des differentes ressources: " + Arrays.toString(Ressource.values());
			throw new FormatException(textError, e);
		}
		checkInt(data, 2, ligne, "L'âge");
		checkInt(data, 4, ligne, "L'absence");
		checkDouble(data, 5, ligne, "La moyenne");
		checkInt(data, 6, ligne, "La motivation");
		checkInt(data, 7, ligne, "L'ânnée");
		checkYear(data, ligne);
	}
	
	public static void checkYear(String[] data, int ligne) throws FormatException {
		int emplacement = 7;
		int year = Integer.parseInt(data[emplacement]);
		if(year < 1 || year > 3) {
			String textError = "L'année " + data[emplacement] + " à la ligne " + ligne + " n'est pas valide";
			textError += FormatException.NEWLINE + "L'année d'étude d'un étudiant doit être compris entre 1 et 3 inclus";
			throw new FormatException(textError);
		}
		if(year > 1 && data.length > 9) {
			String textError = "Le tuteur à la ligne " + ligne + " enseigne plus d'une ressource";
			throw new FormatException(textError);
		}
	}
	
	public static void checkInt(String[] data, int col, int ligne, String value) throws FormatException {
		try {
			Integer.parseInt(data[col]);
		}
		catch (NumberFormatException e) {
			String textError = value + " " + data[col] + " à la ligne " + ligne + " n'est pas un nombre";
			textError += FormatException.NEWLINE + "Rappel du format des données: " + FormatException.DEFAULTFORMAT;
			throw new FormatException(textError, e);
		}
	}
	
	public static void checkDouble(String[] data, int col, int ligne, String value) throws FormatException {
		try {
			Double.parseDouble(data[col]);
		}
		catch (NumberFormatException e) {
			String textError = value + " " + data[col] + " à la ligne " + ligne + " n'est pas un nombre";
			textError += FormatException.NEWLINE + "Rappel du format des données: " + FormatException.DEFAULTFORMAT;
			throw new FormatException(textError, e);
		}
	}
	
	public static void checkMoyenne(String[] data, int ligne) throws FormatException {
		int emplacement = 5;
		double moyenne = Double.parseDouble(data[emplacement]);
		if(moyenne < 0 || moyenne > 20) {
			String textError = data[emplacement] + " à la ligne " + ligne + " n'est pas une moyenne valide";
			textError += FormatException.NEWLINE + "La moyenne d'un étudiant doit être compris entre 0 et 20";
			throw new FormatException(textError);
		}
	}
	
	public static void checkMotivation(String[] data, int ligne) throws FormatException	 {
		int emplacement = 6;
		int motivation = Integer.parseInt(data[emplacement]);
		if(motivation < 0 || motivation > 10) {
			String textError = data[emplacement] + " à la ligne " + ligne + " n'est pas une valeur de motivation valide";
			textError += FormatException.NEWLINE + "La motivation d'un étudiant doit être compris entre 0 et 10";
			throw new FormatException(textError);
		}
	}
	
	
	
}
