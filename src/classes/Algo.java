package classes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ulille.but.sae2_02.graphes.Arete;
import fr.ulille.but.sae2_02.graphes.CalculAffectation ;
import fr.ulille.but.sae2_02.graphes.GrapheNonOrienteValue;
/**
 * 
 */

/**
 * @author YOUNES
 *
 */
public class Algo {
	
	public Map<Tuteur, List<Tutore>> affectation;
	public CalculAffectation<Etudiant> calcul;
	
	public Algo() {
		affectation = new HashMap<>();
	}
	
	/**
	 * 
	 * @param tuteurs Un tuteur avec des valeurs nulles est considéré comme un emplacement vacant
	 * @param tutores Un tutoré avec des valeurs nulles est considéré comme un emplacement vacant
	 */
	public Map<Tuteur, List<Tutore>> affecter(ArrayList<Etudiant> tuteurs, ArrayList<Etudiant> tutores,int[] poids) {
		GrapheNonOrienteValue<Etudiant> graphe = new GrapheNonOrienteValue<>();
		transform(tuteurs, tutores);
		for (Etudiant tuteur : tuteurs) {
			graphe.ajouterSommet(tuteur);
		}
		for (Etudiant tutore : tutores) {
			graphe.ajouterSommet(tutore);
		}
		assignData(tuteurs, tutores, graphe,poids);
		calcul = new CalculAffectation<Etudiant>(graphe, tutores, tuteurs);
		addToMap();
		ArrayList<Etudiant> lonely = getLonely(tutores);
		if(lonely.size() > 0) return affecter(tuteurs, lonely,poids);
		else return affectation;
	}
	
	public void assignData(ArrayList<Etudiant> tuteurs, ArrayList<Etudiant> tutores, GrapheNonOrienteValue<Etudiant> graphe,int[] poids) {
		for (Etudiant tutore : tutores) {
			for (Etudiant tuteur : tuteurs) {
				graphe.ajouterArete(tutore, tuteur, getWeight(tuteur, tutore,poids));
			}
		}
	}
	/**
	 * 
	 * @param tuteur
	 * @param tutore
	 * @param poids
	 * @return 
	 */
	public int getWeight(Etudiant tuteur, Etudiant tutore, int[] poids) {
		if(tuteur.getNom() == null || tutore.getNom() == null) return Integer.MAX_VALUE;
		boolean isPossible = false;
		for (Ressource ressource : tuteur.getRessources()) {
			if(tutore.getRessources().indexOf(ressource) != -1) {
				isPossible = true;
			}
		}
		if(!isPossible) return Integer.MAX_VALUE;
		double gradeValue = tutore.getMoyenne() - tuteur.getMoyenne();
		double yearValue = tuteur.getNiveau();
		double absenceValue = tuteur.getNbAbsences() + tutore.getNbAbsences();
		double motivValue = (tutore.getMotivation()-tutore.getMotivation()*1.5)+(tuteur.getMotivation()-tuteur.getMotivation()*1.5);
		int res = (int) (gradeValue*poids[0] - motivValue*poids[1] + absenceValue*poids[2] - yearValue*poids[3] );
		return res;
	}
	
	
	/**
	 * Enregistre les etudiants affectés dans la hashMap
	 */
	public void addToMap() {
		List<Arete<Etudiant>> list = calcul.getAffectation();
		for (Arete<Etudiant> arete : list) {
			if(affectation.containsKey(arete.getExtremite1()) && arete.getExtremite1().getNom() != null) {
				affectation.get(arete.getExtremite1()).add((Tutore) arete.getExtremite2());
			}
			else if(arete.getExtremite1().getNom() != null && arete.getExtremite2().getNom() != null){
				ArrayList<Tutore> tutores = new ArrayList<>();
				tutores.add((Tutore) arete.getExtremite1());
				affectation.put((Tuteur) arete.getExtremite2(), tutores);
			}
		}
	}
	
	/**
	 * 
	 * @param tutores
	 * @return
	 */
	public ArrayList<Etudiant> getLonely(ArrayList<Etudiant> tutores) {
		ArrayList<Etudiant> res = new ArrayList<>();
		for (Etudiant tutore : tutores) {
			boolean finded = false;
			for (List<Tutore> list : affectation.values()) {
				if(list.contains(tutore)) finded = true;
			}
			if(!finded && tutore.getNom() != null) res.add(tutore);
		}
		return res;
	}
	
	/**
	 * 
	 * @param tuteurs
	 * @param tutores
	 */
	public void transform(ArrayList<Etudiant> tuteurs, ArrayList<Etudiant> tutores) {
		while(tuteurs.size() > tutores.size()) {
			tutores.add(new Tutore(null, null, 0, null));
		}
		while(tutores.size() > tuteurs.size()) {
			tuteurs.add(new TuteurTroisiemeAnnee(null, null, 0, null));
		}
	}
}
