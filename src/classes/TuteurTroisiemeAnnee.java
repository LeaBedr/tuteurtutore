package classes;
import java.util.ArrayList;

public class TuteurTroisiemeAnnee extends Tuteur{
	
	private ArrayList<Tutore> tutores;


	public TuteurTroisiemeAnnee(String nom, String prenom, int age, String mail,int nb, double moy, int motiv, Ressource ressource) {
		super(nom, prenom, age, mail,nb,moy,motiv,3,ressource);	
		tutores=new ArrayList<Tutore>();
	}

	public TuteurTroisiemeAnnee(String nom, String prenom, int age, String mail,int nb, double moy, int motiv) {
		super(nom, prenom, age, mail,nb,moy,motiv);	
		tutores=new ArrayList<Tutore>();
	}

	public TuteurTroisiemeAnnee(String nom, String prenom, int age, String mail) {
		super(nom, prenom, age, mail);	

		tutores=new ArrayList<Tutore>();
	}
	

	@Override
	public ArrayList<Tutore> getTutores() {
		return tutores;
	}

	@Override
	public void addTutore(Tutore t) {
		tutores.add(t);
		t.addTuteur(this);
	}

}
