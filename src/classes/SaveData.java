package classes;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 
 */

/**
 * @author Younes Chahid
 * @version 13 mai 2022 15:32:34
 */
public class SaveData {
	/**
	 * 
	 */
	private static final File MAPFILE = new File("res/affectation");
	private static final File STUDENTFILE = new File("res/students");
	private static final String SEPARATOR = " % ";
	private static final String TAB = "\t";
	
	
	public static void saveMap(Map<Tuteur, List<Tutore>> affectation) {
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(MAPFILE))) {
			oos.writeObject(affectation);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void saveMap(Algo algo, File file) throws IOException {
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
			Map<Tuteur, List<Tutore>> map = algo.affectation;
			Set<Entry<Tuteur, List<Tutore>>> toSave = map.entrySet();
			StringBuilder sb = new StringBuilder();
			for (Entry<Tuteur, List<Tutore>> entry : toSave) {
				StringBuilder tuteurData = new StringBuilder();
				StringBuilder tutoreData = new StringBuilder();
				String[] rawTuteurData = entry.getKey().getRawData().split(",");
				for (String data : rawTuteurData) {
					tuteurData.append(data + SEPARATOR);
				}
				tuteurData.append(FormatException.NEWLINE);
				List<Tutore> tutores = entry.getValue();
				for (Tutore tutore : tutores) {
					tutoreData.append(TAB);
					String[] rawTutoreData = tutore.getRawData().split(",");
					for (String data : rawTutoreData) {
						tutoreData.append(data + SEPARATOR);
					}
					tutoreData.append(FormatException.NEWLINE);
				}
				sb.append(tuteurData.toString());
				sb.append(tutoreData.toString());
			}
			writer.write(sb.toString());
		}
	}
	
	public static Map<Tuteur, List<Tutore>> loadMap() {
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(MAPFILE))) {
			return (Map<Tuteur, List<Tutore>>) ois.readObject();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(FileNotFoundException e) {
			// pass handle useless
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static void saveStudents(List<Tuteur> tuteurs, List<Tutore> tutores) {
		ArrayList<Etudiant> etudiants = new ArrayList<>();
		for (Tuteur tuteur : tuteurs) {
			etudiants.add(tuteur);
		}
		for (Tutore tutore : tutores) {
			etudiants.add(tutore);
		}
		saveStudents(etudiants);
	}
	
	public static void saveStudents(List<Etudiant> etudiants) {
		try(BufferedWriter studentsFileWriter = new BufferedWriter(new FileWriter(STUDENTFILE))) {
			for (Etudiant etudiant : etudiants) {
				String save = etudiant.getNom();
				save += "," + etudiant.getPrenom();
				save += "," + etudiant.getAge();
				save += "," + etudiant.getMail();
				save += "," + etudiant.getNbAbsences();
				save += "," + etudiant.getMoyenne();
				save += "," + etudiant.getMotivation();
				save += "," + etudiant.getNiveau();
				for (Ressource ressource : etudiant.getRessources()) {
					save += "," + ressource.name();
				}
				studentsFileWriter.write(save);
				studentsFileWriter.newLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
