package classes;

public enum Ressource {
	BDD, POO, GRAPHES, STATS, C, RESEAU, SYSTEME, ARCHITECTURE, 
	OUTILS, MATHS, IJAVA, ANGLAIS, DROIT, GESTION, ECONOMIE, 
	QUALITE, GPO, METHODESNUM;
	
	public String toString() {
		return this.name();
	}
}
