package classes;
import java.util.ArrayList;

import fr.ulille.but.sae2_02.donnees.DonneesPourTester;

/**
 * 
 */

/**
 * @author Younes Chahid
 *
 */
public class UseAlgo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Algo test = new Algo();
		String[][] data = new String[][]{
			{"Younes", "chahid", }
		};
		ArrayList<Etudiant> tutores = new ArrayList<>();
		ArrayList<Etudiant> tuteurs = new ArrayList<>();
		for (String[] studentData : data) {
			if(studentData[3].equals("1")) {
				tutores.add(new Tutore(studentData[1], studentData[0], 18, null, 0, Double.parseDouble(studentData[2]), 0, 1));
			}
			else {
				//tuteurs.add(new TuteurTroisiemeAnnee(studentData[1], studentData[0], 18, null, 0, Double.parseDouble(studentData[2]), 0, Integer.parseInt(studentData[3])));
			}
		}
		System.out.println(test.affecter(tuteurs, tutores));

	}

}
