package classes;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * 
 */

/**
 * @author Younes Chahid
 * @version 16 mai 2022 11:29:07
 */
public class Main {
	
	static Enseignant enseignant;
	static Scanner sc = new Scanner(System.in);
	static Algo algo;
	static ArrayList<Etudiant> tuteurs;
	static ArrayList<Etudiant> tutores;
	static int[] poids = new int[] {1, 1, 1, 1};
	private static final String[] METHODS_NAME = new String[] {
		"affecter",
		"editWeight",
		"addStudent",
		"importFromFile",
		"sauvegarderAffectation",
		"showStudents",
		"deleteStudent",
		"validerUnEtudiant"
	};
	private static final Method[] METHODS = new Method[METHODS_NAME.length];
			
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		addMethods();
		algo = new Algo();
		algo.affectation = new HashMap<>();
		tutores = new ArrayList<>();
		tuteurs = new ArrayList<>();
		program();
	}
	
	/**
	 * 
	 */
	private static void addMethods() {
		// TODO add other menu methods if any
		addMainMenuMethods();
	}
	
	private static void addMainMenuMethods() {
		try {
			Method[] methods = MainMenuMethods.class.getDeclaredMethods();
			List<String> methodsNameList = Arrays.asList(METHODS_NAME);
			for (Method method : methods) {
				int index = methodsNameList.indexOf(method.getName()) ;
				if(index != -1) {
					Main.METHODS[index] = method;
				}
			}
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static void program() {
		bienvenue();
		connection();
		while(true) {
			clear();
			mainMenu();
			String res = read();
			try {
				int menuButton = Integer.parseInt(res);
				if(menuButton < 1 || menuButton > METHODS.length) {
					System.out.println("Vous devez entrer un nombre compris entre 1 et " + METHODS.length);
					sleep(3);
				}
				else {
					try {
						METHODS[menuButton - 1].invoke(null);
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			}
			catch (NumberFormatException e) {
				System.out.println("Vous n'avez pas entré de nombre");
				sleep(3);
			}
		}
		
	}
	
	/**
	 * 
	 */
	static void clear() {
		for (int i = 0; i < 100; i++) {
			System.out.println();
		}
	}

	/**
	 * 
	 */
	static void mainMenu() {
		// TODO Auto-generated method stub
		System.out.println("1. Affecter les étudiants avec ceux actuels");
		System.out.println("2. Modifier les poids de l'affectattion");
		System.out.println("3. Ajouter un nouvel étudiant");
		System.out.println("4. Ajouter plusieurs étudiants depuis un fichier");
		System.out.println("5. Sauvegarder l'affectation");
		System.out.println("6. Afficher les étudiants");
		System.out.println("7. Supprimer un étudiant");
		System.out.println("8. Valider un étudiant");
	}

	/**
	 * 
	 */
	private static void connection() {
		System.out.println("Veuillez enregistrer les ressources dont vous êtes responsable");
		System.out.println("Terminez votre saisie par \"stop\"");
		System.out.println(Arrays.toString(Ressource.values()));
		ArrayList<Ressource> ressources = getRessource();
		System.out.println("Veuillez entrer votre nom puis votre prénom");
		String[] names = getNames();
		System.out.println("Veuillez entrer votre âge");
		int age = getAge();
		System.out.println("Veuillez entrer votre mail");
		String mail = getMail();
		enseignant = new Enseignant(names[0], names[1], age, mail, ressources);
	}
	
	static String getMail() {
		return read();
	}
	
	static int getAge() {
		try {
			String response = read();
			return Integer.parseInt(response);
		}
		catch(NumberFormatException e) {
			System.out.println("Vous n'avez pas entré de nomobre, veuillez réessayer");
			return getAge();
		}
	}
	
	static String[] getNames() {
		String[] res = new String[2];
		res[0] = read();
		res[1] = read();
		return res;
	}
	
	private static ArrayList<Ressource> getRessource() {
		ArrayList<Ressource> res = new ArrayList<Ressource>();
		String response = "";
		while(!(response = read().toUpperCase()).equals("STOP")) {
			try {
				res.add(Ressource.valueOf(response));
			}
			catch(IllegalArgumentException e) {
				// Traiter cet erreur est inutile
				// il suffit simplement de recommencer la boucle
			}
		}
		return res;
	}

	static void bienvenue() {
		// TODO enter exit to stop the program
		System.out.println("Bienvenue dans le programme d'affectation de tuteurs - tutorés");
		System.out.println("Entrez exit pour arreter le programme a n'importe quel moment");
	}
	
	static void sleep(int time) {
		try {
			TimeUnit.SECONDS.sleep(time);
		} catch (InterruptedException e) {
			// TODO mettre un break
			e.printStackTrace();
		}
	}
	
	static String read() {
		String res = sc.nextLine();
		if(res.toLowerCase().equals("exit")) {
			System.out.println("Arret du programme");
			System.exit(0);
		}
		return res;
	}

}
