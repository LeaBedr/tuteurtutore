package classes;
import java.util.ArrayList;

public class Enseignant extends Personne {
	private ArrayList<Ressource> enseigne;
	
	public ArrayList<Ressource> getEnseigne() {
		return enseigne;
	}

	Enseignant(String nom, String prenom, int age, String mail, ArrayList<Ressource> enseigne){
		super(nom, prenom, age, mail);
		this.enseigne=enseigne;
	}
	
	public Enseignant(String nom, String prenom, int age, String mail){
		this(nom, prenom, age, mail, new ArrayList<Ressource>());
	}
	
	public void addRessource(Ressource r) {
		enseigne.add(r);
	}
	
	public void addRessource(String r) {
		enseigne.add(Ressource.valueOf(r.toUpperCase())); 
	}
	
	
	
	private String enseigne() {
		String res="";
		for(Ressource r: enseigne) {
			res+=r.toString()+" ";
		}
		return res;
	}
	
	public void valider(Etudiant e) {
		boolean valid = false;
		for (Ressource ressource : e.getRessources()) {
			if(enseigne.contains(ressource)) {
				valid = true;
			}
		}
		if(valid) e.setValide(true);
	}
	
	public void invalider(Etudiant e) {
		boolean valid = false;
		for (Ressource ressource : e.getRessources()) {
			if(enseigne.contains(ressource)) {
				valid = true;
			}
		}
		if(valid) e.setValide(false);
	}
	
	public void affecterTutore(Tuteur tuteur, Tutore tutore) {
		if (tuteur.getValide()) {
			tuteur.addTutore(tutore);
			tutore.addTuteur(tuteur);
		}
	}
	
	public String toString() {
		return "Enseignant: "+super.toString() + ", enseigne: " + enseigne();
	}
}
