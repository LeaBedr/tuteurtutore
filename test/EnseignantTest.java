import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import classes.Enseignant;
import classes.Ressource;
import classes.Tuteur;
import classes.TuteurDeuxiemeAnnee;
import classes.TuteurTroisiemeAnnee;
import classes.Tutore;



/**
 * 
 */

/**
 * DEV 
 *
 * @author Lea Bedrici
 * 9 mai 2022
 */
class EnseignantTest {
	
	Tutore tutore1, tutore2, tutore3;
	Tuteur tuteur1, tuteur2, tuteur3, tuteur4;
	Enseignant enseignant1, enseignant2, enseignant3;
	
	
	@BeforeEach
	void init() {
		tutore1=new Tutore("Lea", "Bedrici", 18, "lea.bedrici.etu@univ-lille.fr",0,15.2,7,1);
		tutore2=new Tutore("Paul-Louis", "Gomis", 18, "paul-louis.gomis.etu@univ-lille.fr", 0,15.2,0,1);
		tutore3=new Tutore("Younes", "Chahid", 18, "younes.chahid.etu@univ-lille.fr");
		tuteur1=new TuteurTroisiemeAnnee("Jean", "Dupont", 20, "jean.dupont.etu@univ-lille.fr");
		tuteur2=new TuteurTroisiemeAnnee("Louis", "Dubois", 20, "louis.dubois.etu@univ-lille.fr");
		tuteur3=new TuteurTroisiemeAnnee("Mathilde", "Rico", 20, "mathilde.rico.etu@univ-lille.fr");
		tuteur4=new TuteurDeuxiemeAnnee("Charlie","Nelson",19,"charlie.nelson.etu@univ-lille.fr");
		enseignant1 = new Enseignant("Antoine", "Nongaillard", 40, "antoire.nongaillard.ens@univ-lille.fr");
		enseignant2 = new Enseignant("Cindy", "Cappelle", 35, "cindy.cappelle.ens@univ-lille.fr");
		enseignant3 = new Enseignant("Iovka", "Boneva", 37, "iovka.boneva.ens@univ-lille.fr");
		

		
		tutore1.addAbsence(3);
		tutore1.addRessource(Ressource.GPO);
		tutore1.addRessource("maths");
		tutore2.setMotivation(7);
		tutore2.addRessource("anglais");
		tutore3.setMoyenne(15.5);
		tutore3.setMotivation(7);
		tutore3.setNiveau(1);
		tutore3.addAbsence(2);
		tutore3.addRessource("ecoNoMIE");
		
		tuteur2.setNiveau(3);
	}

	/**
	 * Test method for {@link Enseignant#addRessource(Ressource)}.
	 */
	@Test
	void testAddRessourceRessource() {
		enseignant1.addRessource(Ressource.POO);
		enseignant1.addRessource(Ressource.BDD);
		
		ArrayList<Ressource> donnees=new ArrayList<Ressource>();
		donnees.add(Ressource.POO);
		donnees.add(Ressource.BDD);
		assertEquals(donnees, enseignant1.getEnseigne());
		donnees.clear();
		assertEquals(donnees, enseignant3.getEnseigne());
		
	}

	/**
	 * Test method for {@link Enseignant#addRessource(java.lang.String)}.
	 */
	@Test
	void testAddRessourceString() {

		enseignant2.addRessource("ijava");
		ArrayList<Ressource> donnees2=new ArrayList<Ressource>();
		donnees2.add(Ressource.IJAVA);
		assertEquals(donnees2, enseignant2.getEnseigne());
	}

	/**
	 * Test method for {@link Enseignant#valider(Tuteur)}.
	 */
	@Test
	void testValiderTuteur() {
		enseignant1.addRessource(Ressource.POO);
		tuteur2.setRessource(Ressource.IJAVA);
		tuteur3.setRessource("poo");
		enseignant1.valider(tuteur3);
		assertTrue(tuteur3.getValide());
		enseignant3.valider(tuteur2);
		assertFalse(tuteur2.getValide());
	}

	/**
	 * Test method for {@link Enseignant#invalider(Tuteur)}.
	 */
	@Test
	void testInvaliderTuteur() {
		enseignant1.addRessource(Ressource.POO);
		tuteur2.setRessource(Ressource.IJAVA);
		tuteur3.setRessource("poo");
		enseignant1.valider(tuteur3);
		enseignant3.valider(tuteur2);
		enseignant3.invalider(tuteur3);
		assertTrue(tuteur3.getValide());
		enseignant1.invalider(tuteur3);
		assertFalse(tuteur3.getValide());
		
	}

	/**
	 * Test method for {@link Enseignant#valider(Tutore)}.
	 */
	@Test
	void testValiderTutore() {
		enseignant1.addRessource(Ressource.GRAPHES);
		enseignant2.addRessource(Ressource.GESTION);
		tutore1.addRessource(Ressource.GRAPHES);
		tutore3.addRessource("outils");
		enseignant1.valider(tutore1);
		enseignant2.valider(tutore3);
		assertTrue(tutore1.getValide());
		assertFalse(tutore3.getValide());
	}

	/**
	 * Test method for {@link Enseignant#invalider(Tutore)}.
	 */
	@Test
	void testInvaliderTutore() {
		enseignant1.addRessource(Ressource.GRAPHES);
		enseignant2.addRessource(Ressource.GESTION);
		tutore1.addRessource(Ressource.GRAPHES);
		tutore3.addRessource("outils");
		enseignant1.valider(tutore1);
		enseignant2.valider(tutore3);
		enseignant2.invalider(tutore1);
		assertTrue(tutore1.getValide());
		enseignant1.invalider(tutore1);
		assertFalse(tutore1.getValide());
	}

	/**
	 * Test method for {@link Enseignant#affecterTutore(Tuteur, Tutore)}.
	 */
	@Test
	void testAffecterTutore() {	
		enseignant1.addRessource(Ressource.GRAPHES);
		enseignant2.addRessource(Ressource.MATHS);
		enseignant2.addRessource(Ressource.OUTILS);
		tutore1.addRessource(Ressource.GRAPHES);
		tutore2.addRessource("maths");
		tutore3.addRessource(Ressource.OUTILS);
		enseignant1.valider(tutore1);
		enseignant2.valider(tutore3);
		enseignant2.valider(tutore2);
		tuteur1.setRessource(Ressource.GRAPHES);
		tuteur2.setRessource("maths");
		tuteur3.setRessource("outils");
		enseignant1.valider(tuteur1);
		enseignant2.valider(tuteur2);
		enseignant2.valider(tuteur3);
		enseignant2.affecterTutore(tuteur2, tutore2);
		HashMap<Tuteur,Ressource> donnees=new HashMap<Tuteur,Ressource>();
		donnees.put(tuteur2, Ressource.MATHS);
		assertEquals(donnees, tutore2.getTuteurs());	
		
	}
	
	/**
	 * Test method for {@link Enseignant#TuteursDeuxiemeAnnee}.
	 */

	@Test
	void testTuteursDeuxiemeAnnee() {
		enseignant1.addRessource(Ressource.GRAPHES);
		tutore1.addRessource(Ressource.GRAPHES);
		enseignant1.valider(tutore1);
		tuteur4.setRessource(Ressource.GRAPHES);
		enseignant1.valider(tuteur4);
		enseignant1.affecterTutore(tuteur4, tutore1);
		assertTrue(tuteur4.getValide());
		assertEquals(tutore1,tuteur4.getTutores().get(0));
		HashMap<Tuteur,Ressource> donnees=new HashMap<Tuteur,Ressource>();
		donnees.put(tuteur4, Ressource.GRAPHES);
		assertEquals(donnees,tutore1.getTuteurs());
		tuteur4.addTutore(tutore2);
		assertEquals(new HashMap<Tuteur,Ressource>(),tutore1.getTuteurs());
	}
}
