import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import classes.Algo;
import classes.Etudiant;
import classes.Tuteur;
import classes.TuteurDeuxiemeAnnee;
import classes.TuteurTroisiemeAnnee;
import classes.Tutore;
import fr.ulille.but.sae2_02.graphes.Arete;
import fr.ulille.but.sae2_02.graphes.CalculAffectation;
import fr.ulille.but.sae2_02.graphes.GrapheNonOrienteValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;

/**
 * @author Younes Chahid
 * @version 5 mai 2022 11:21:46
 */
class AlgoTest {
	
	ArrayList<Tutore> tutores;
	ArrayList<Tuteur> tuteurs;
	ArrayList<Etudiant> tutoresATester;
	ArrayList<Etudiant> tuteursATester;
	Tuteur tuteur1, tuteur2, tuteur3, tuteur4, tuteur5;
	Tutore tutore1, tutore2, tutore3, tutore4, tutore5;
	Algo algo;
	
	
	@BeforeEach
	void init() {
		tutore1 = new Tutore("Chahid", "Younes", 18, "younes.chahid.etu", 0, 0, 0, 1);
		tutore2 = new Tutore("Bedrici", "lea", 18, "lea.bedrici.etu", 0, 5, 0, 1);
		tutore3 = new Tutore(null, null, 0, null);
		tutore4 = new Tutore("Chevrette", "Roslyn", 18, "roslyn.chevrette.etu", 0, 10, 0, 1);
		tutore5 = new Tutore("allard", "chantal", 18, "chantal.alard.etu", 0, 7, 0, 1);
		
		
		
		tuteur1 = new TuteurTroisiemeAnnee("Dupont", "Pierre", 20, "pierre.dupont.etu", 0, 20, 0);
		tuteur2 = new TuteurDeuxiemeAnnee("Jean", "paul", 19, "paul.jean.etu", 0, 20, 0);
		tuteur3 = new TuteurTroisiemeAnnee("Dupuis", "Pierre", 20, "pierre.dupuis.etu", 0, 15, 0);
		tuteur4 = new TuteurDeuxiemeAnnee("Veronneau", "leroy", 20, "leroy.veronneau", 0, 5, 0);
		tuteur5 = new TuteurTroisiemeAnnee("Marcoux", "Louis", 19, "louis.marcoux.etu", 0, 0, 0);
		
		
		
		tutores = new ArrayList<Tutore>();
		tuteurs = new ArrayList<Tuteur>();
		tutoresATester = new ArrayList<Etudiant>();
		tuteursATester = new ArrayList<Etudiant>();
		tutoresATester.add(tutore1);
		tutoresATester.add(tutore2);
		tutoresATester.add(tutore3);
		tuteursATester.add(tuteur1);
		tuteursATester.add(tuteur2);
		tuteursATester.add(tuteur3);
		algo = new Algo();
	}
	


	@Test
	void testAffecter() {
		tuteursATester.add(tuteur4);
		tuteursATester.add(tuteur5);
		tutoresATester.remove(2);
		tutoresATester.add(tutore4);
		tutoresATester.add(tutore5);
		Map<Tuteur, List<Tutore>> affectation = algo.affecter(tuteursATester, tutoresATester, new int[]{1,1,1,1});
		assertEquals(tutore1, affectation.get(tuteur1).get(0));
		assertEquals(tutore5, affectation.get(tuteur2).get(0));
		assertEquals(tutore4, affectation.get(tuteur3).get(0));
		assertFalse(affectation.containsKey(tuteur5));
		assertEquals(4, affectation.size());
	}
	
	/**
	 * Test method for {@link Algo#assignData(java.util.ArrayList, java.util.ArrayList, fr.ulille.but.sae2_02.graphes.GrapheNonOrienteValue)}.
	 */
	@Test
	void testAssignData() {
		GrapheNonOrienteValue<Etudiant> graphe = new GrapheNonOrienteValue<>();
		for (Etudiant tuteur : tuteursATester) {
			graphe.ajouterSommet(tuteur);
		}
		for (Etudiant tutore : tutoresATester) {
			graphe.ajouterSommet(tutore);
		}
		algo.assignData(tuteursATester, tutoresATester, graphe, new int[]{1,1,1,1});
		assertTrue(graphe.contientArete(tutore1, tuteur2));
		assertEquals(-23, graphe.getPoids(tutore1, tuteur1));
	}

	/**
	 * Test method for {@link Algo#getWeight(Etudiant, Etudiant)}.
	 */
	@Test
	void testGetWeight() {
		int[] poids = new int[]{1,1,1,1};
		assertEquals(Integer.MAX_VALUE, algo.getWeight(tuteur1, tutore3,poids));
		assertEquals(-23, algo.getWeight(tuteur1, tutore1,poids));
		assertEquals(-17, algo.getWeight(tuteur2, tutore2,poids));
	}

	/**
	 * Test method for {@link Algo#addToMap()}.
	 */
	@Test
	void testAddToMap() {
		GrapheNonOrienteValue<Etudiant> graphe = new GrapheNonOrienteValue<>();
		for (Etudiant tuteur : tuteursATester) {
			graphe.ajouterSommet(tuteur);
		}
		for (Etudiant tutore : tutoresATester) {
			graphe.ajouterSommet(tutore);
		}
		algo.assignData(tuteursATester, tutoresATester, graphe, new int[]{1,1,1,1});
		algo.calcul = new CalculAffectation<>(graphe, tutoresATester, tuteursATester);
		algo.addToMap();
		assertTrue(algo.affectation.containsKey(tuteur1));
		assertTrue(algo.affectation.containsKey(tuteur2));
		assertFalse(algo.affectation.containsKey(tuteur3));
	}

	/**
	 * Test method for {@link Algo#getLonely(java.util.ArrayList)}.
	 */
	@Test
	void testGetLonely() {
		GrapheNonOrienteValue<Etudiant> graphe = new GrapheNonOrienteValue<>();
		for (Etudiant tuteur : tuteursATester) {
			graphe.ajouterSommet(tuteur);
		}
		for (Etudiant tutore : tutoresATester) {
			graphe.ajouterSommet(tutore);
		}
		algo.assignData(tuteursATester, tutoresATester, graphe, new int[]{1,1,1,1});
		algo.calcul = new CalculAffectation<>(graphe, tutoresATester, tuteursATester);
		algo.addToMap();
		ArrayList<Etudiant> lonely = algo.getLonely(tutoresATester);
		assertEquals(0, lonely.size());
	}

	/**
	 * Test method for {@link Algo#transform(java.util.ArrayList, java.util.ArrayList)}.
	 */
	@Test
	void testTransform() {
		tutoresATester.remove(2);
		algo.transform(tuteursATester, tutoresATester);
		assertEquals(3, tutoresATester.size());
		assertEquals(tutore3.getNom(), tutoresATester.get(2).getNom());
	}

}
